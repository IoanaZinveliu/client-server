
#include<stdio.h>       //biblioteca standard de intrare/iesire
#include<sys/socket.h> //utilizate pt functia socket
#include<sys/types.h>
#include<netdb.h>
#include<string.h>
#include<unistd.h>
#include "client.c"
int main(){

	printf("Serverul s-a deschis\n");

	//socket
int sockfd;
sockfd = socket(AF_INET, SOCK_STREAM,0);  
//ofera o interfata de programare intre procese aflate pe dispozitive diferite
//AF_INET=>IPv4, SOCK_STREAM=> type, '0'=>protocol TCP
if(sockfd == -1){
	printf("Socket creat gresit!\n");
		return 0;
}
else{
	printf("Socket creat cu succes!\n");
}

	//bind
struct addrinfo hints, *res;
memset(&hints,0,sizeof hints); //initializare memoriei din hints cu 0
hints.ai_family = AF_INET;
hints.ai_socktype = SOCK_STREAM;
hints.ai_flags = AI_PASSIVE;     //adresa va fi folosita de un server care asteapta conexiuni

getaddrinfo(NULL,"22323", &hints, &res);
//NULL=>va furniza info despre adresa si portul pe care va asculta serverul
//22323=>portul pe care asculta serverul
//&hints=> ofera sugestii despre tipul de adresa si de socket
//&res=>unde vor fi stocate informatiile oferite de getaddrinfo


// tratare eroare TIME_WAIT
int yes=1; //utilizata la activarea SO_REUSEADDR din cadrul setsockopt


if(setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,&yes,sizeof yes) == -1){ 
	//setsockopt permite utlizarea imediata a unei adrese chiar daca aceasta se afla in time wait
	perror("setsockopt");//afiseaza mesaj de eroare relevant
	return 0;
}

if(bind(sockfd, res->ai_addr, res->ai_addrlen)==-1 ){ //bind-leaga un socket la o adresa si un port
	printf("Functia bind nu functioneaza!\n");
	return 0;
}
else{
	printf("Functia bind functioneaza!\n");
}

	//listen

if(listen(sockfd,10) == -1){
	//10-backlog,nr conexiuni care pot sa stea in coada
	//specifica faptul ca sockfd va fi folosit pt aasculta conexiuni pe server
	printf("Functia listen nu functioneaza!\n");
	return 0;
}
else{
	printf("Functia listen functioneaza!\n");
}

	//accept

int new_fd; //se creeaza un socket nou intre server si clientul acceptat
struct sockaddr_storage their_addr; //stocheaza informatii despre adresa clientului care s-a conectat
socklen_t addr_size; //dimensiunea structurii

do{
addr_size = sizeof their_addr;
new_fd = accept(sockfd, (struct sockaddr *)&their_addr, &addr_size);//parametrii:socket, adresa unde vor merge info despre conexiune,dimensiunea adresei
//accept-accepta conexiunea stabilita prin connect
if(new_fd == -1){
	printf("Functia accept nu functioneaza!\n");
	return 0;
}
else{
	printf("Functia accept functioneaza!\n");
}

	//receive-primeste datele de la clientul conectat prin socket

int byte_count;
char buf[512];

do{

byte_count = recv(new_fd, buf, sizeof buf, 0);//0-nu folosim optiuni suplimentare
buf[byte_count] = '\0'; //pt a ne asigura ca toate datele sunt afisate
printf("%s\n",buf);

if(byte_count == -1){
	printf("Functia recive nu functioneaza!\n");
	return 0;
}
else{
	printf("Functia recive functioneaza!\n");
}
	//send-trimite un mesaj clientului conectat prin socket

char *secret_message = "Comanda neimplementata";
int receive; //semnalizeaza comanda pe care am implementat-o
if(strcmp(buf,"05#")==0){
printf("Comanda implementata\n");
 receive=1;

}
else {
	printf("Comanda neimplementata!\n");
	receive=0;
}

if(send(new_fd,secret_message, strlen(secret_message)+1,0) == -1){//0-flag
	printf("Functia send nu functioneaza!\n");
	return 0;
}
else{
	printf("Functia send functioneaza!\n");
}
int bytes_send;
if(byte_count>0){

	if(receive==0){
	//daca comanda nu e"#05" =>comanda neimplementata
	bytes_send=send(new_fd,secret_message, strlen(secret_message),0);
	}
	else{
		client();
		
		FILE *file;
		file=fopen("output.txt","r");
		char line[500];
	while(!feof(file)){
		
		fgets(line,500,file);
		char *msg=line;
		bytes_send=send(new_fd, msg,strlen(msg),0);

	}
	//transmite linie cu linie in fisier
	fclose(file);

	}


}
}while(byte_count>0);





}while(1);


freeaddrinfo(res);
close(sockfd);
close(new_fd);
return 0;
}



