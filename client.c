#include<stdio.h>
#include<string.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netdb.h>
#include<unistd.h>
#include<stdlib.h>

void client( ){

printf("Programul s-a deschis!\n");

//functia socket
int sockfd;

sockfd=socket(AF_INET6,SOCK_STREAM, 0);//IPv6,type stream, TCP
if(sockfd==-1){
	printf("Socket creat gresit\n");
        exit(1);
}
	else{
	printf("Socket creat cu succes\n");
	}


//functia getaddrinfo
struct addrinfo hints, *res;
memset(&hints,0, sizeof hints);
hints.ai_family = AF_INET6;
hints.ai_socktype = SOCK_STREAM;

if(getaddrinfo("he.net","http", &hints, &res)==0){//he.net- nume host, http-port 80
	printf("Functia getaddrinfo functioneaza!\n");
}
	else{
	printf("Functia getaddrinfo nu functioneaza!\n");
	exit(1);
	}

//functia connect
if(connect(sockfd,res->ai_addr,res->ai_addrlen)==-1){
	printf("Functia connect nu functioneaza!\n");
        exit(1);
}
	else{
	printf("Functia connect functioneaza!\n");

	}

//functia send
char *secret_message="GET / HTTP/1.0\r\n\r\n";
//GET-obtinere info, /-calea de baza, HTTP/1.0-versiune protocol, \r\n\r\n-capat de cerere(linie goala)
if(send(sockfd, secret_message,strlen(secret_message)+1,0)==-1){

	printf("Functia send nu functioneaza!\n");
	exit(1);
}
	else{
	printf("Functia send functioneaza!\n");
	}


//functia receive
int byte_count;
char buf[512];
FILE *fp;
fp=fopen("output.txt","w");
while((byte_count=recv(sockfd,buf,sizeof buf,0))>0){
	//printf("Numarul de bytes=  %d \n", byte_count);
	buf[byte_count]='\0';
	fprintf(fp,"%s",buf);
	//fwrite(buf,strlen(buf),1,fp);
	//scriem tot ce receptionam de la host in fisierul output.txt
	}

if(byte_count==-1){
 	printf("Functia receive nu functioneaza!\n");
        exit(1);
}

else{
printf("Functia receive functioneaza\n");

}


fclose(fp);
}



